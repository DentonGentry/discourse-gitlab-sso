#!/usr/bin/env python3
"""
Discourse SSO authentication using JupyterHub and Gitlab.

This is intended to provide single-sign-on support for a Discourse forum
(https://meta.discourse.org/t/official-single-sign-on-for-discourse-sso/13045)
which is associated with a JupyterHub instance, allowing users visiting
the forum for the first time to have their identity and profile be
automatically populated from information in JupyterHub and Gitlab.

This is expected to run as a JupyterHub Managed Service.
(https://jupyterhub.readthedocs.io/en/stable/reference/services.html)

The following environment variables must be provided when this service is launched:
    'GITLAB_CLIENT_ID', 'GITLAB_CLIENT_SECRET': hex strings copied from the
       Application ID and Secret fields of the gitlab authentication app.
       (https://docs.gitlab.com/ee/integration/oauth_provider.html)
    'EXTERNAL_BASE_URL': like 'https://jupyterhub.example.com',
       The externally-reachable DNS name for this service. gitlab.com's
       OAuth callback is redirected to a path on this URL, it has to be
       reachable from gitlab.com's servers.
       Note that JUPYTERHUB_BASE_URL is automatically provided by JupyterHub
       when running as a managed service, but in testing JupyterHub was providing
       http://127.0.0.1/ which isn't going to work.
    'DISCOURSE_SECRET': a hex string which matches the secret from Discourse SSO
       (https://meta.discourse.org/t/official-single-sign-on-for-discourse-sso/13045)
       It is recommended that 'openssl rand -hex 32' be used to generate this.

For example if using The Littleest JupyterHub (https://http://tljh.jupyter.org)
the following would be in /opt/tljh/config/jupyterhub_config.d/discourse-service.py
c.JupyterHub.services = [
    {
        'name': 'discourse',
        'url': 'http://172.17.0.2:80/',
        'api_token': 'no_token',
    },
    {
        'name': 'discourse-sso',
        'url': 'http://127.0.0.1:10101',
        'command': ['/opt/tljh/user/bin/flask', 'run', '--port=10101'],
        'environment': {'FLASK_APP': '/opt/tljh/hub/bin/discourse-sso.py',
            'GITLAB_CLIENT_ID': '...',
            'GITLAB_CLIENT_SECRET': '...',
            'EXTERNAL_BASE_URL': 'https://jupyterhub.example.com',
            'DISCOURSE_SECRET': '...',
        },
    },
]


The following code examples were very helpful in generating this implementation:
    https://gist.github.com/kemitche/9749639
    https://github.com/jupyterhub/jupyterhub/tree/master/examples/service-whoami-flask
    https://gist.github.com/dkess/cec370a6d76ac11020f1424de646ba1d
"""
import base64
import binascii
import functools
import hashlib
import hmac
import json
import os
import pprint
import urllib.parse
import uuid

import flask
import requests
import requests.auth


app = flask.Flask(__name__)
client_states = {}
SERVICE_PREFIX = os.environ.get('JUPYTERHUB_SERVICE_PREFIX', '/')
EXTERNAL_BASE_URL = os.environ.get('EXTERNAL_BASE_URL', 'missing EXTERNAL_BASE_URL')


# For debugging, uncomment the @app.route line and visit
# https://jupyterhub.example.com/services/discourse-sso/debug-gitlab
#@app.route(SERVICE_PREFIX + 'debug-gitlab')
def debug_gitlab():
    state_id = str(uuid.uuid4())
    client_states[state_id] = {'nonce': 'debug-gitlab', 'return_sso_url': ''}
    redirect_url = gitlab_get_authorize_url(state_id=state_id)
    return flask.redirect(redirect_url, code=302)


# ------------- Gitlab OAuth2 support -------------

GITLAB_CLIENT_ID = os.environ.get('GITLAB_CLIENT_ID', '')
GITLAB_CLIENT_SECRET = os.environ.get('GITLAB_CLIENT_SECRET', '')
GITLAB_AUTHORIZE_URL = 'https://www.gitlab.com/oauth/authorize'
GITLAB_ACCESS_TOKEN_URL = 'https://www.gitlab.com/oauth/token'
GITLAB_API_URL = 'https://gitlab.com/api/v4'
LOCAL_REDIRECT_URI = SERVICE_PREFIX + 'gitlab_oauth_callback'

@app.route(LOCAL_REDIRECT_URI)
def gitlab_oauth_callback():
    """Endpoint which gitlab.com will redirect the user's browser to after authorization."""
    orig_callback_args = flask.request.args.copy()
    token_json = {}
    user_json = {}
    ds_user = {}
    error = flask.request.args.get('error', '')
    if error:
        return 'Error: ' + error
    state_id = flask.request.args.get('state', '')
    state = client_states.get(state_id, {})
    if not state:
        flask.abort(403)
        return
    code = flask.request.args.get('code')

    # Exchange the one-time-use code for an OAuth token
    client_auth = requests.auth.HTTPBasicAuth(GITLAB_CLIENT_ID, GITLAB_CLIENT_SECRET)
    post_data = {
            'client_id': GITLAB_CLIENT_ID,
            'client_secret': GITLAB_CLIENT_SECRET,
            'code': code,
            'grant_type': 'authorization_code',
            'redirect_uri': EXTERNAL_BASE_URL + LOCAL_REDIRECT_URI,
            }
    response = requests.post(GITLAB_ACCESS_TOKEN_URL, auth=client_auth, data=post_data)
    token_json = response.json()
    access_token = token_json.get('access_token', None)
    if access_token:
        # Fetch user attributes
        url = f'{GITLAB_API_URL}/user?access_token={access_token}'
        response = requests.get(url, auth=client_auth, data=post_data)
        user_json = response.json()

    if user_json:
        ds_user = gitlab_user_to_discourse(gh_user_data=user_json, state=state)

    del client_states[state_id]

    return_sso_url = state.get('return_sso_url', '')
    if return_sso_url:
        # Redirect browser back to Discourse
        encoded = urllib.parse.urlencode(ds_user)
        base64encoded = base64.b64encode(encoded.encode())
        sso_sig = {
            'sso': base64encoded.decode(),
            'sig': binascii.hexlify(h(base64encoded)).decode(),
        }

        redirect_url = return_sso_url + '?' + urllib.parse.urlencode(sso_sig)
        return flask.redirect(redirect_url, code=302)
    else:
        # Display data locally
        output = 'callback: ' + pprint.pformat(orig_callback_args, indent=1) + '\n\n'
        output += 'token: ' + pprint.pformat(token_json, indent=1) + '\n\n'
        output += 'gitlab_user: ' + pprint.pformat(user_json, indent=1) + '\n\n'
        output += 'discourse_user: ' + pprint.pformat(ds_user, indent=1) + '\n\n'
        return flask.Response(output, mimetype='application/json')


def gitlab_get_authorize_url(state_id):
    params = {
        'client_id': GITLAB_CLIENT_ID,
        'scope': 'read_user',
        'response_type': 'code',
        'state': state_id,
        'redirect_uri': EXTERNAL_BASE_URL + LOCAL_REDIRECT_URI,
        'duration': 'temporary',
    }
    return GITLAB_AUTHORIZE_URL + '?' + urllib.parse.urlencode(params)


# -------------- Discourse support ----------------


DISCOURSE_SECRET = os.environ.get('DISCOURSE_SECRET', '').encode('utf-8')


def h(msg):
    return hmac.new(DISCOURSE_SECRET, msg, digestmod=hashlib.sha256).digest()


@app.route(SERVICE_PREFIX + 'sso')
def discourse_sso():
    sso = flask.request.args.get('sso')
    sig = flask.request.args.get('sig')
    got_sig = h(sso.encode())

    if not hmac.compare_digest(binascii.unhexlify(sig), got_sig):
        flask.abort(400)

    sso_parse = urllib.parse.parse_qs(base64.b64decode(sso).decode())
    state_id = str(uuid.uuid4())
    client_states[state_id] = {
        'nonce': sso_parse['nonce'][0],
        'return_sso_url': sso_parse['return_sso_url'][0],
        }
    redirect_url = gitlab_get_authorize_url(state_id=state_id)
    return flask.redirect(redirect_url, code=302)


def gitlab_user_to_discourse(gh_user_data, state):
    ds_user = {
        'nonce': state.get('nonce', ''),
        'email': gh_user_data.get('email', ''),
        'require_activation': 'false',
        'external_id': 'gitlab_com_id_' + str(gh_user_data.get('id', 0)),
        'username': gh_user_data.get('username', ''),
    }
    if gh_user_data.get('name', None):
        ds_user['name'] = gh_user_data['name']
    if gh_user_data.get('avatar_url', None):
        ds_user['avatar_url'] = gh_user_data['avatar_url']
        ds_user['avatar_force_update'] = 'true'
    if gh_user_data.get('bio', None):
        ds_user['bio'] = gh_user_data['bio']
    if gh_user_data.get('website_url', None):
        ds_user['website'] = gh_user_data['website_url']
    return ds_user
