# discourse-gitlab-sso
Discourse SSO authentication using JupyterHub and Gitlab.

This is intended to provide [single-sign-on support for a Discourse forum](https://meta.discourse.org/t/official-single-sign-on-for-discourse-sso/13045) which is associated with a JupyterHub instance, allowing users visiting the forum for the first time to have their identity and profile be automatically populated from information in JupyterHub and Gitlab.

This is expected to run as a [JupyterHub Managed Service.](https://jupyterhub.readthedocs.io/en/stable/reference/services.html)

The following environment variables must be provided when this service is launched:
+  'GITLAB_CLIENT_ID', 'GITLAB_CLIENT_SECRET': hex strings copied from the Application ID and Secret fields of the [gitlab authentication app.](https://docs.gitlab.com/ee/integration/oauth_provider.html)

+  'EXTERNAL_BASE_URL': like "https://jupyterhub.example.com", The externally-reachable DNS name for this service. gitlab.com's OAuth callback is redirected to a path on this URL, it has to be reachable from gitlab.com's servers.  
Note that JUPYTERHUB_BASE_URL is automatically provided by JupyterHub when running as a managed service, but in testing JupyterHub was providing http://127.0.0.1/ which isn't going to work.

+  'DISCOURSE_SECRET': a hex string which matches the [secret from Discourse SSO](https://meta.discourse.org/t/official-single-sign-on-for-discourse-sso/13045). It is recommended that 'openssl rand -hex 32' be used to generate this.

For example if using The Littlest JupyterHub (https://tljh.jupyter.org) the following would be in /opt/tljh/config/jupyterhub_config.d/discourse-service.py  

    c.JupyterHub.services = [
        {
            'name': 'forum',
            'url': 'http://172.17.0.2:80/',
            'api_token': 'no_token',
        },
        {
            'name': 'discourse-sso',
            'url': 'http://127.0.0.1:10101',
            'command': ['/opt/tljh/user/bin/flask', 'run', '--port=10101'],
            'environment': {'FLASK_APP': '/opt/tljh/hub/bin/discourse-sso.py',
                'GITLAB_CLIENT_ID': '...',
                'GITLAB_CLIENT_SECRET': '...',
                'EXTERNAL_BASE_URL': 'https://jupyterhub.example.com',
                'DISCOURSE_SECRET': '...',
            },
        },
    ]


The following code examples were very helpful in generating this implementation:
+  https://gist.github.com/kemitche/9749639
+  https://github.com/jupyterhub/jupyterhub/tree/master/examples/service-whoami-flask
+  https://gist.github.com/dkess/cec370a6d76ac11020f1424de646ba1d
